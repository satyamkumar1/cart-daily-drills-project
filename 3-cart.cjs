const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/


// Q1. Find all the items with price more than $65.


function getItemWithMorePrice65() {

    const expensiveItems = [];
    const getItem = products.map((data) => {

        Object.values(data).map((item) => {

            if (typeof item.price === 'string' && Number(item.price.slice(1)) > 65) {
                return expensiveItems.push(item);
            }
            else if (Array.isArray(item)) {

                item.map((data) => {
                    //  console.log(Object.values(data)[0]);
                    const priceData = Object.values(data)[0].price;
                    if (parseInt(priceData.slice(1)) > 65) {
                        return expensiveItems.push(Object.values(data)[0]);
                    }
                });

            }

        });

    });

    console.log(expensiveItems);


}
getItemWithMorePrice65();


// Q2. Find all the items where quantity ordered is more than 1.

function getQuantityOfItem() {


    const quantityItems = [];
    const getQuantityItem = products.map((data) => {

        Object.values(data).map((item) => {

            if (item.quantity > 1) {
                return quantityItems.push(item);
            }
            else if (Array.isArray(item)) {

                item.map((data) => {
                    //  console.log(Object.values(data)[0]);
                    const quantityData = Object.values(data)[0].quantity;
                    if (quantityData > 1) {
                        return quantityItems.push(Object.values(data)[0]);
                    }
                });

            }

        });

    });

    console.log(quantityItems);


}
getQuantityOfItem();


// Q.3 Get all items which are mentioned as fragile.


function getItemOfFragile() {

    const fragileItems = [];
    const getFragileItem = products.map((data) => {

        Object.values(data).map((item) => {

            if (item.type === "fragile") {
                return fragileItems.push(item);
            }
            else if (Array.isArray(item)) {

                item.map((data) => {
                    //  console.log(Object.values(data)[0]);
                    const typeData = Object.values(data)[0].type;
                    if (typeData === "fragile") {
                        return fragileItems.push(Object.values(data)[0]);
                    }
                });

            }

        });

    });

    console.log(fragileItems);




}
getItemOfFragile();


// Q.4 Find the least and the most expensive item for a single quantity.

function getLeastAndMostExpensiveItem() {


    const getLeastAndMostExpensive = [];
    const getLeastItem = products.map((data) => {

        Object.values(data).map((item) => {

            if (typeof item === 'object') {
                return getLeastAndMostExpensive.push(item);
            }
            else if (Array.isArray(item)) {

                item.map((data) => {
                    //  console.log(Object.values(data)[0]);
                    const typeData = Object.values(data)[0];
                    console.log(typeof typeData)
                    if (typeof typeData === 'object') {
                        return getLeastAndMostExpensive.push(Object.values(data)[0]);
                    }
                });

            }

        });

    });


    getLeastAndMostExpensive.sort((prev,curr)=>{

        const prevPrice=Number(prev.slice(1));
        const currPrice=Number(curr.slice(1));


    })

    console.log(getLeastAndMostExpensive);




}
getLeastAndMostExpensiveItem();
